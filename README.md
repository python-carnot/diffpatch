# diffpatch

L'objectif de ce mini projet est la création
de deux programmes:

* `diff.py` permet de créer un patch permettant de transformer un fichier 1 à un fichier 2.
* `patch.py` réalise l'opération inverse: on applique le patch précédent à fichier 1 pour recréer fichier2.

## Création du patch

```nil
python diff.py fichier1.py fichier2.py > fichier1-vers-2.patch
```

Le fichier `fichier1-vers-2.patch` est au format JSON:

```json
[
  {
    "op": "-",
    "start": 2,
    "lignes": [
      "    c = x * x\n",
      "    return c\n"
    ]
  },
  {
    "op": "+",
    "start": 2,
    "lignes": [
      "    return x**2\n"
    ]
  },
  {
    "op": "-",
    "start": 5,
    "lignes": [
      "print(carré(-1))\n"
    ]
  },
  {
    "op": "+",
    "start": 7,
    "lignes": [
      "print(carré(7))\n"
    ]
  }
]
```

## Application du patch

```nil
cat fichier1-vers-2.patch | python patch.py fichier1.py > nouveau-fichier2.py
```

Les contenus de `fichier2.py` et `nouveau-fichier2.py` doivent être identiques.
