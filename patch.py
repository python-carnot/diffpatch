import sys
import json

def applique_patch(fichier1, patch):
   fichier2 = [ligne for ligne in fichier1]
   
   for opération in patch:
      if opération["op"] == "-":
         début = opération["start"]
         fichier2.pop(début - 1)
         for op in patch:
            if op["start"] > début:
               op["start"] -= 1
      else:
         assert opération["op"] == '+'
         début = opération["start"]
         fichier2.insert(début - 1, opération["lignes"][0])
         for op in patch:
            if op["start"] >= début:
               op["start"] += 1

   return fichier2
   
if __name__ == '__main__':
   nom1 = sys.argv[1]

   with open(nom1) as f1:
      lignes1 = f1.readlines()
   
   patch = json.load(sys.stdin)
   
   lignes2 = applique_patch(lignes1, patch)
   for l in lignes2:
      sys.stdout.write(l)
