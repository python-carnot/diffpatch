def appariement(mot1, mot2):
   N1 = len(mot1) + 1
   N2 = len(mot2) + 1

   # Algorithme en programmation dynamique
   # Création de la matrice d'appariement   
   matrice = [[0] * N2 for _ in range(N1)]
   for i in range(N1):
      for j in range(N2):
         if i == 0: # première ligne
            matrice[i][j] = -j
         elif j == 0: # première colonne
            matrice[i][j] = -i
         else:
            # on vient du haut
            score = matrice[i-1][j] - 1
            
            # on regarde depuis la gauche
            score = max(score, matrice[i][j-1] - 1)
            
            # puis on regarde depuis la diagonale
            if mot1[i - 1] == mot2[j - 1]:
               score = max(score, matrice[i-1][j-1] + 1)
            else:
               score = max(score, matrice[i-1][j-1] - 1)
            
            matrice[i][j] = score
   
   # Utilisation de la matrice pour créer l'appariement
   app1 = []
   app2 = []
   
   # On part de la dernière case en bas à droite
   i = N1 - 1
   j = N2 - 1
   
   # Tant qu'on n'est pas arrivé en haut à gauche
   while i > 0 or j > 0:
      if i == 0: # première ligne
         # on vient forcément de la gauche
         
         app2.insert(0, mot2[j-1])
         app1.insert(0, None)
         
         j -= 1
      elif j == 0: # première colonne
         # on vient forcémenent du haut
         
         app2.insert(0, None)
         app1.insert(0, mot1[i-1])
         
         i -= 1
      else:
         # On vient d'une des 3 directions possibles
         # On choisit la première qui donne le bon score
         
         score = matrice[i][j]
         if score == matrice[i-1][j] - 1:
            # haut possible ?
            
            app2.insert(0, None)
            app1.insert(0, mot1[i-1])
            
            i -= 1
         elif score == matrice[i][j-1] - 1:
            # gauche possible ?

            app2.insert(0, mot2[j-1])
            app1.insert(0, None)
            
            j -= 1
         else:
            # on vient forcément de la diagonale

            app1.insert(0, mot1[i-1])
            app2.insert(0, mot2[j-1])
            
            i -= 1
            j -= 1         

   #print(app1)
   #print(app2)
   
   position = 1
   for i in range(len(app1)):
      if app1[i] == None:
         print("AJOUTER en {}: {}".format(position, app2[i]))
      elif app2[i] == None:
         print("SUPPRIMER en {}: {}".format(position, app1[i]))
         position += 1
      elif app1[i] != app2[i]:
         #print("REMPLACER en {}: {} -> {}".format(position, app1[i], app2[i]))
         print("SUPPRIMER en {}: {}".format(position, app1[i]))
         print("AJOUTER en {}: {}".format(position, app2[i]))
         position += 1
      else: # app1[i] == app2[i]:
         position += 1        

prog1 = [
   "def carré(x):",
   "    c = x * x",
   "    return c",
   "",
   "print(carré(-1))",
   "print(carré(3))"
]
   
prog2 = [
   "pi = 3.14",
   "e = 2.718",
   "phi = 1.618",
   ""
   "def carré(x):",
   "    return x**2",
   "",
   "print(carré(3))",
   "print(carré(7))",
]
   
appariement(prog1, prog2)

