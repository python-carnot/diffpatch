import sys

print(sys.argv)

nom1 = sys.argv[1]
nom2 = sys.argv[2]

with open(nom1) as f1:
   lignes1 = f1.readlines()
   
with open(nom2) as f2:
   lignes2 = f2.readlines()
   
print("Contenu de {}:".format(nom1))
print(lignes1)

print()
print("Contenu de {}:".format(nom2))
print(lignes2)

