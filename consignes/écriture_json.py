diff = [
  { "op": "-",
    "start": 1,
    "lines": ["    return x * x"]
  },
  { "op": "+",
    "start": 1,
    "lines": ["    return x**2"]
  },
  { "op": "-",
    "start": 3,
    "lines": ["print(carré(-1))"]
  },
  { "op": "+",
    "start": 5,
    "lines": ["print(carré(7))"]
  }
]

import json
import sys

sys.stderr.write("Écriture du contenu diff\n")
json.dump(diff, sys.stdout, ensure_ascii=False, indent=2)
   

